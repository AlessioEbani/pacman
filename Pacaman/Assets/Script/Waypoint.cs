﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waypoint : MonoBehaviour
{
    private Teleport t;

    private void Awake() {
        t = GetComponentInParent<Teleport>();
        if (!t) {
            Debug.LogError("Teleport not found in " + name);
        }
    }

    //if trigger A, teleport to B e viceversa 
    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.CompareTag("Player") || collision.gameObject.CompareTag("Enemy")) {
            if (transform.Equals(t.GetA())) {
                collision.transform.position = t.GetArriveB().position;
            }
            if (transform.Equals(t.GetB())) {
                collision.transform.position = t.GetArriveA().position;
            }
        }
    }
}
