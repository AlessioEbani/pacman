﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private SoundManager sfx;
    private LifeManager lm;
    private DotManager dm;
    private GameObject gameOver;
    private GameObject ending;
    private EnemyMovment[] enemies;
    private PacmanMovement pacman;

    private float globalVel = 1;

    public float standardVel = 1;
    public float eatableVel = 0.4f;
    
    public float superDotTime = 3;
    public bool superDot = false;
    

    private float time;

    private void Awake() {
        globalVel = standardVel;
        gameOver = GameObject.FindGameObjectWithTag("GameOver");
        ending = GameObject.FindGameObjectWithTag("Ending");
        sfx = FindObjectOfType<SoundManager>();
        dm = GetComponent<DotManager>();
        enemies = FindObjectsOfType<EnemyMovment>();
        lm = FindObjectOfType<LifeManager>();
        var pac = GameObject.FindGameObjectWithTag("Player");
        pacman = pac.GetComponent<PacmanMovement>();
        if (!pacman) {
            Debug.LogError("Pacman Sbagliato");
        }
        if (!ending) {
            Debug.LogError("Missing Game over title" + name);
        }
        if (!gameOver) {
            Debug.LogError("Missing Game over title" + name);
        }
        if (!dm) {
            Debug.LogError("Dot manager missing " + name);
        }
        if (!lm) {
            Debug.LogError("lm not found in " + name);
        }
        if (!sfx) {
            Debug.LogError("Sound manager not found in " + name);
        }
        gameOver.SetActive(false);
        ending.SetActive(false);
        Time.timeScale = 0;
        sfx.Play(0);
    }

    private void Update() {
        //wait until beginner's music is ended (uau che inglish)
        if (sfx.audiosrc[0].isPlaying) {
            Time.timeScale = 0;
        }else if (sfx.audiosrc[2].isPlaying) { //wait until Death music is ended for respawn
            Time.timeScale = 0;
        }else if (sfx.audiosrc[3].isPlaying) { //wait until eat music is ended
            Time.timeScale = 0;
        } else {
            Time.timeScale = 1;
        }
        //loose condition
        if (lm.HasLost()) {
            Time.timeScale = 0;
            gameOver.SetActive(true);
        }
        //win condition
        if (dm.HasWon()) {
            Time.timeScale = 0;
            ending.SetActive(true);
        }

        //timer del super dot
        if (superDot) {
            time += Time.deltaTime;
            if (time > superDotTime) {
                EndSuperDot();
                time = 0;
            }
        }

        //debug
        if (Input.GetKeyDown(KeyCode.R)) {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
        if (Input.GetKeyDown(KeyCode.Escape)) {
            Application.Quit();
        }
    }


    public void ActiveSuperDot() {
        foreach(EnemyMovment e in enemies) {
            e.setEatable(true);
            e.animator.SetBool("Eatable", true);
            globalVel = eatableVel;
            time = 0;
            superDot = true;
        }
    }

    public void EndSuperDot() {
        foreach (EnemyMovment e in enemies) {
            e.setEatable(false);
            e.animator.SetBool("Eatable", false);
            globalVel = standardVel;
            superDot = false;
        }
    }

    public float GetGlobalVel() {
        return globalVel;
    }

    public SoundManager GetSoundManager() {
        return sfx;
    }

    public LifeManager GetLifeManager() {
        return lm;
    }

    public PacmanMovement GetPacman() {
        return pacman;
    }
}
