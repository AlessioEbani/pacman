﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class MapManager : MonoBehaviour
{

    Tilemap tilemap;
    Grid grid;
    GameObject player;
    public List<Tile> map;
    Tile players;

    [Serializable]
    public class Tile{
        public Vector3Int position;
        public string tag;
        public Tile(Vector3Int position, string tag){
            this.position = position;
            this.tag = tag;
        }
    }


    private void Awake() {
        grid = FindObjectOfType<Grid>();
        if (!grid) {
            Debug.LogError("Grid not found in " + name);
        }

        player = GameObject.FindGameObjectWithTag("Player");
        if (!player) {
            Debug.Log("player not found in " + name);
        }
        var tile = FindObjectsOfType<Tilemap>();
        //find the tilemap with the walls
        foreach(Tilemap maps in tile) {
            if (maps.CompareTag("Wall")) {
                tilemap = maps;
                break;
            }
        }
        if (!tilemap) {
            Debug.LogError("Tilemap wall not found in " + name);
        }
        map = new List<Tile>();
        //find each posisition of the tilmap walls and add into the list, the value
        for(int i=0; i < tilemap.size.y; i++) {
            for(int j=0; j < tilemap.size.x; j++) {
                var tiles = tilemap.GetTile(new Vector3Int(j, i, 0));
                if (tiles != null) {
                    map.Add(new Tile(new Vector3Int(j,i,0),"Wall"));
                }
            }
        }
        //add into the list the initial posision of the player
        var playerPosition=grid.LocalToCell(player.transform.position);
        players = new Tile(playerPosition, "Player");
        map.Add(players);
    }



    public String GetTileTag(Vector3Int position) {
        String tag = "";
        foreach(Tile t in map) {
            if (t.position.Equals(position)) {
                tag = t.tag;
                break;
            }
        }
        return tag;
    }

    
    public bool CanMove(String name,Vector3Int dir) {
        bool can = true;
        foreach (Tile t in map) {
            if (t.tag.Equals(name)) {
                var newPosition = t.position + dir*2;
                var tag = GetTileTag(newPosition);
                if (tag.Equals("Wall")) {
                    can = false;
                } else if(tag.Equals("")) {
                    Debug.Log("C'è qualcosa che non quadra");
                }
            }
        }
        return can;
    }

    public bool Move(String name,Vector3Int dir) {
        bool ok = false;
        if (CanMove(name, dir)) {
            players.position += dir;
            ok = true;
        }
        return ok;
    }

    public void RemoveFromList(Vector3Int position) {
        Tile tile=new Tile(position,"");
        foreach (Tile t in map) {
            if (t.position.Equals(position)) {
                tile = t;
            }
        }
        map.Remove(tile);
    }

    private void Update() {
    }

    private void OnDrawGizmos() {
        foreach (Tile t in map) {
            Gizmos.DrawCube(tilemap.WorldToLocal(t.position), new Vector3(0.1f,0.1f, 0));
        }
    }
}
