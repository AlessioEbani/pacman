﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DotManager : MonoBehaviour
{
    private GameObject dots;
    private Transform[] allDots;
    private SoundManager sfx;

    private int size;

    private void Awake() {
        dots = GameObject.FindGameObjectWithTag("AllDots");
        sfx = FindObjectOfType<SoundManager>();
        if (!dots) {
            Debug.LogError("Dots not found in "+name);
        }
        if (!sfx) {
            Debug.LogError("Sound manager not found in " + name);
        }

        allDots = dots.GetComponentsInChildren<Transform>();
        size = allDots.Length-1; //-1 perchè prende anche il padre;
    }

    

    public void Eat() {
        if (!sfx.audiosrc[1].isPlaying) {
            sfx.Play(1);
        }
        size--;
    }

    public bool HasWon() {
        if (size <= 0) {
            return true;
        }
        return false;
    }
}
