﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuperDot : Dot
{
    private GameManager gm;

    private void Awake() {
        Init();
        gm = FindObjectOfType<GameManager>();
        if (!gm) {
            Debug.LogError("Missing Gm in "+name);
        }
    }


    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.CompareTag("Player")) {
            dm.Eat();
            Destroy(this.gameObject);
            gm.ActiveSuperDot();
        }
    }
}
