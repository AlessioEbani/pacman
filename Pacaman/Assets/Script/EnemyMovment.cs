﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovment : PacmanMovement
{
    //roba che non va
    //public float distance;
    private bool canChangeDirection=false;

    private bool eatable=false;
    private SpriteRenderer sprite;
    

    public float spawnTime;
    private float time;
    
    private void Awake() {
        Init();
        sprite = GetComponent<SpriteRenderer>();
        if (!sprite) {
            Debug.LogError("Missing sprite in" + name);
        }
    }

    public void Update() {
        if (!isDead) {
            velocity = gm.GetGlobalVel();
            MyUpdate();
        } else {
            time += Time.deltaTime;
            if (time >= spawnTime) {
                time = 0;
                Respawn();
            }
        }
        
    }

    //get a random direction when it can't go forward
    public override Vector3 GetDirection() {
        Vector3 direction = dir;
        if (!CanMove(direction) || canChangeDirection) {
            bool ok = false;
            var newDir = direction;
            while (!ok) {
                int random = Random.Range(1, 5);
                switch(random){
                    case 1: 
                        newDir = Vector3.up;
                        break;
                    case 2:
                        newDir = Vector3.down;
                        break;
                    case 3:
                        newDir = Vector3.right;
                        break;
                    case 4:
                        newDir = Vector3.left;
                        break;
                }
                if (CanMove(newDir)) {
                    ok = true;
                }
                if (Input.GetKeyDown(KeyCode.Space)) {
                    ok = true;
                }
            }
            direction = newDir;
        }

        return direction;
    }

    //try to do the change direction 
    //private void OnTriggerEnter2D(Collider2D collision) {
    //    if (collision.gameObject.CompareTag("Change")) {
    //        var distanceX=Mathf.Abs(transform.position.x- collision.transform.position.x);
    //        var distanceY = Mathf.Abs(transform.position.y - collision.transform.position.y);
    //        if (distanceX <= distance && distanceY <= distance && !canChangeDirection) {
    //            canChangeDirection = true;
    //        } 
    //    }
        
    //}

    private void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.CompareTag("Player")) {
            if (eatable) {
                Kill();
            } else {
                gm.GetPacman().Kill();
            }
        }
    }

    public override void Kill() {
        isDead = true;
        colliders.enabled = false;
        sprite.enabled = false;
        transform.position = new Vector3(0, 0.2f, 0);
        gm.GetSoundManager().Play(3);
    }

    //when the ghost respawn
    public override void Respawn() {
        isDead = false;
        transform.position = initialPos;
        dir = Vector3.right;
        colliders.enabled = true;
        sprite.enabled = true;
    }

    public void setEatable(bool eat) {
        eatable = eat;
    }
}
