﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeManager : MonoBehaviour
{
    private GameObject[] lives;
    private int count;

    private void Awake() {
        lives = GameObject.FindGameObjectsWithTag("Life");
        count = lives.Length;
    }

    public void Respawn() {
        count--;
        lives[count].SetActive(false);
    }

    public bool HasLost() {
        if(count <= 0) {
            return true;
        }
        return false;
    }
}
