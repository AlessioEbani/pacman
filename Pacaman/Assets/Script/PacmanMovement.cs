﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PacmanMovement : MonoBehaviour
{

    protected Vector3 initialPos;
    protected Vector3 dir;
    public float velocity = 1;
    protected bool isDead;

    public Animator animator;
    protected BoxCollider2D colliders;
    protected LayerMask layer;
    protected GameManager gm;

    private void Awake() {
        Init();
    }

    //setting iniziale
    protected void Init() {
        initialPos = transform.position;
        dir = Vector3Int.right;
        layer = 1 << 8;
        gm = FindObjectOfType<GameManager>();
        animator = GetComponent<Animator>();
        colliders = GetComponent<BoxCollider2D>();
        if (!animator) {
            Debug.LogError("Errore animator in "+ name);
        }
        if (!colliders) {
            Debug.LogError("Missing box collider 2d in " + name);
        }
        if (!gm) {
            Debug.LogError("Gm not found in " + name);
        }
    }
    

    private void Update()
    {
        if (isDead) {
            if (!gm.GetSoundManager().audiosrc[2].isPlaying) {
                Respawn();
                gm.GetLifeManager().Respawn();
            }
        } else {
            MyUpdate();
        }
    }

    protected void MyUpdate() {
        dir = GetDirection();
        animator.SetFloat("DirX", dir.x);
        animator.SetFloat("DirY", dir.y);
        transform.position += velocity * dir * Time.deltaTime;
    }

    public virtual Vector3 GetDirection() {
        
        Vector3 direction = dir;
        if (!CanMove(direction)) {
            direction = Vector3.zero;
        }

        if (Input.GetKey(KeyCode.W)) {
            if (CanMove(Vector2.up)) {
                direction = Vector2.up;
            } 
        }
        if (Input.GetKey(KeyCode.S)) {
            if (CanMove(-Vector2.up)) {
                direction = -Vector2.up;
            }
        }
        if (Input.GetKey(KeyCode.D)) {
            if (CanMove(Vector2.right)) {
                direction = Vector2.right;
            } 
        }
        if (Input.GetKey(KeyCode.A)) {
            if (CanMove(Vector2.left)) {
                direction = Vector2.left;
            }

        }
        direction.Normalize();
        return direction;
    }

    //physics raycast to check if the move is possible
    public bool CanMove(Vector2 dir) {
        bool ok = true;
        var boxExtests = colliders.bounds.extents;
        if (Physics2D.BoxCast(transform.position,boxExtests, 0, dir,boxExtests.x*7/4, layer)) {
            ok = false;
        }
        return ok;
    }


    public virtual void Kill() {
        isDead = true;
        animator.SetBool("IsDead", true);
        gm.GetSoundManager().Play(2);
    }

    public virtual void Respawn() {
        isDead = false;
        transform.position = initialPos;
        dir = Vector3.right;
        animator.SetBool("IsDead", false);
    }

}
