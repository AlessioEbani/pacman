﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour
{
    private Transform A;
    private Transform B;
    private Transform arriveA;
    private Transform arriveB;


    private void Awake() {
        //sorry for the hardcode :/
        var waypoint = GetComponentsInChildren<Transform>();
        A = waypoint[1];
        arriveA = waypoint[2];
        B = waypoint[3];
        arriveB = waypoint[4];
        if(!A || !B) {
            Debug.LogError("Missing A or B in " + name);
        }
    }


    public Transform GetA() {
        return A;
    }
    public Transform GetB() {
        return B;
    }
    public Transform GetArriveA() {
        return arriveA;
    }
    public Transform GetArriveB() {
        return arriveB;
    }
}
