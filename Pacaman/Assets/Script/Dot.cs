﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dot : MonoBehaviour
{
    protected DotManager dm;

    private void Awake() {
        Init();
    }

    protected void Init() {
        dm = FindObjectOfType<DotManager>();
        if (!dm) {
            Debug.LogError("Dot manager missing in " + name);
        }
    }


    private void OnTriggerEnter2D(Collider2D other) {
        MyTrigger(other);
    }

    protected void MyTrigger(Collider2D other) {
        if (other.gameObject.CompareTag("Player")) {
            dm.Eat();
            Destroy(this.gameObject);
        }
    }
}
